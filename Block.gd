extends Node2D

const N = 1
const S = 2
const E = 3
const W = 4

var sprite_texture: String = ""
var connections: Dictionary = {}

# connections
# type: position
# N: Vector3(7, 4)

func _ready() -> void:
	if not sprite_texture.empty():
		$Sprite.texture = load(sprite_texture)

func load_json(json_path: String) -> int:
	return 0

func save_json(json_path: String) -> int:
	return 0
