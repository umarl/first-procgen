extends Reference

var file_path: String = ""

var doors: Dictionary = {}
var rooms: Dictionary = {}

const Door = preload("Door.gd")
const Room = preload("Room.gd")

# loads a json file containing 
func load_collection(path: String) -> int:
	# create file object
	var f = File.new()
	
	# check if file exists
	if not f.file_exists(path):
		f.free()
		print("File doesn't exist: %s" % path)
		return ERR_DOES_NOT_EXIST
	
	# check extension
	if path.get_extension().to_lower() != "json":
		f.free()
		print("File is not a json file: %s" % path)
		return ERR_CANT_OPEN
	
	# open the file
	var err = f.open(path, File.READ)
	if err != OK:
		f.free()
		print("Error opening file: %s" % path)
		return err
	
	# get file as text
	var s = f.get_as_text()
	f.close()
	f.free()
	
	# parse json
	var parsed = JSON.parse(s)
	if parsed.error != OK:
		print("Error opening file, %s: %s" % [parsed.error_string, path])
		return parsed.error
	
	# check if parsed json is dictionary
	if typeof(parsed.result) != TYPE_DICTIONARY:
		print("Invalid file: %s" % path)
		return ERR_FILE_UNRECOGNIZED
	
	# check if dictionary has necessary fields
	if not parsed.result.has_all(["doors", "rooms"]):
		print("Invalid file: %s" % path)
		return ERR_FILE_UNRECOGNIZED
	
	# no errors, so file is valid
	file_path = path
	var c = parsed.result
	
	# create doors
	for door in c["doors"].keys():
		var d = Door.new()
		d.set_id(int(door))
		for connection in c["doors"][door]:
			d.add_connection(int(connection))
		doors[d.get_id()] = d
	
	# create rooms
	for room in c["rooms"].keys():
		var r = Room.new()
		r.load_dictionary(int(room), c["rooms"][room])
		rooms[r.get_id()] = r
	
	return OK

# saves the collection as a json into the disk
func save_collection() -> int:
	# create File object
	var f = File.new()
	
	# open file
	var err = f.open(file_path, File.WRITE)
	if err != OK:
		f.free()
		print("Error saving file: %s" % file_path)
		return err
	
	# get all doors
	var door_dict = {}
	for door in doors.keys():
		door_dict[door] = door.get_connections()
	
	# get all rooms
	var room_dict = {}
	for room in rooms.keys():
		room_dict[room] = room.get_dictionary()
	
	# create dictionary
	var c := {}
	c["doors"] = door_dict
	c["rooms"] = room_dict
	
	# save file as a json string
	f.store_text(JSON.print(c))
	f.close()
	f.free()
	
	return OK
