extends Reference

var _id: int = 0
var _connections: Array = []
var _door_name: String = ""
var _color: String = Color.gray.to_html()


func get_id() -> int:
	return _id

func set_id(id: int) -> void:
	_id = id


func get_connections() -> Array:
	return _connections
	
func set_connections(connections: Array) -> void:
	for c in connections:
		add_connection(c)

func add_connection(connection_id: int) -> void:
	if connection_id in _connections:
		return
	_connections.append(connection_id)

func remove_connection(connection_id: int) -> void:
	_connections.erase(connection_id)


func set_door_name(door_name: String) -> void:
	_door_name = door_name

func get_door_name() -> String:
	return _door_name

func set_door_color(html_color_string: String) -> void:
	# should be a html color string
	_color = html_color_string

func get_door_color() -> String:
	return _color
