extends Reference

var _id: int
var texture_path: String = ""
var texture: Texture = null
var doors: Array = []

const Door = preload("Door.gd")

# func setup_room(id: int, info: Dictionary) -> void:
# 	_id = id
	
# 	texture_path = info["texture"]
# 	if not load_texture():
# 		print("Could not load %s of room %d" %[texture_path, _id])
# 		return
	
# 	# for door in info["doors"].keys():
# 		# doors.append(RoomDoor.new(door))
		


func set_id(id: int) -> void:
	_id = id	

func get_id() -> int:
	return _id


func add_door(door_id: int, position: Vector2) -> void:
	doors.append(RoomDoor.new(door_id, position))

func remove_door(door: RoomDoor) -> void:
	doors.erase(door)

func get_doors() -> Array:
	return doors

func load_texture() -> bool:
	# check if resource exists
	if ResourceLoader.exists(texture_path, "Texture") or ResourceLoader.exists(texture_path, "Image"):
		texture = ResourceLoader.load(texture_path)
		return true
	return false
		
func get_texture() -> Texture:
	return texture

func set_texture_path(path: String) -> void:
	texture_path = path

func get_texture_path() -> String:
	return texture_path


func load_dictionary(id: int, dict: Dictionary) -> void:
	# id is passed separately because it's used as the key on the dictionary/json file
	_id = id
	
	texture_path = dict["texture"]
	
	# add doors
	for door in dict["doors"]:
		add_door(int(door["id"]), Vector2(door["x"], door["y"]))

func get_dictionary() -> Dictionary:
	var dict = {}
	dict["texture"] = texture_path
	
	var d = []
	for door in doors:
		d.append({"id": door.get_id(), "x": door.get_position().x, "y": door.get_position().y})
	
	dict["doors"] = d
	
	return dict
	

class RoomDoor extends Reference:
	var _id: int = -1
	var _position: Vector2 = Vector2.ZERO
	var _used: bool = false
	
	func _init(id: int, position: Vector2) -> void:
		self._id = id
		self._position = position
	
	func get_id() -> int:
		return _id
	
	func get_position() -> Vector2:
		return _position
	
	func set_used(used: bool) -> void:
		_used = used
	
	func is_used() -> bool:
		return _used
