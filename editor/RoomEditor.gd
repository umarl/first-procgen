extends Control

# var undo_redo = UndoRedo.new()

onready var AddRoomDialog = $AddRoomDialog
onready var RoomList = $panel/vbox/hsplit/vbox_rooms/scroll_list/RoomList
onready var VP = $panel/vbox/hsplit/room_canvas/VPContainer/viewport/VP
onready var DialogError: AcceptDialog = $DialogError


func _ready() -> void:
	set_process_input(true)


# shows an alert on screen
func show_error_message(message: String, title: String = "Error") -> void:
	DialogError.dialog_text = message
	DialogError.window_title = title
	DialogError.popup_centered()

# ------------- UI SIGNALS -------------#

# hbox_menu nodes

func _on_ButtonNew_pressed() -> void:
	pass # Replace with function body.


func _on_ButtonOpen_pressed() -> void:
	pass # Replace with function body.


func _on_ButtonSave_pressed() -> void:
	pass # Replace with function body.


# room list buttons (?)

func _on_ButtonAddRoom_pressed() -> void:
	AddRoomDialog.popup_centered()


func _on_ButtonRemoveRoom_pressed() -> void:
	for item in RoomList.get_selected_items():
		RoomList.remove_item(item)
	RoomList.unselect_all()


# room list

func _on_RoomList_item_activated(index: int) -> void:
	$panel/vbox/hsplit/room_canvas/VPContainer/hbox/RoomName.text = RoomList.get_item_text(index)
	VP.set_sprite(RoomList.get_item_icon(index))


# viewport container
func _on_ButtonCenterCanvas_pressed() -> void:
	VP.set_camera_pos(Vector2(0, 0))


func _on_ButtonResetZoom_pressed() -> void:
	VP.set_zoom(1)


# Dialogs
func _on_AddRoomDialog_files_selected(paths: PoolStringArray) -> void:
	var valids = []
	
	for p in paths:
		if ResourceLoader.exists(p, "Texture") or ResourceLoader.exists(p, "Image"):
			valids.append(p)
	
	if valids.empty():
		print('invalid')
		show_error_message("No valid files selected")
		return
	
	for f in valids:
		var texture = ResourceLoader.load(f)
		var text = f.get_file().replace(f.get_extension(), "")
		text = text.substr(0, text.length() - 1)
		RoomList.add_item(text, texture)
