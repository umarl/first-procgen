extends ViewportContainer


func _ready() -> void:
	set_process_input(true)

# Forward inputs to viewport(s)
func _on_VPContainer_gui_input(event: InputEvent) -> void:
#	var x_form = get_global_transform() # seems to be necessary with 3d worlds
	var x_form = Transform2D()
	
	if stretch:
		var scale_xf = Transform2D()
		scale_xf.scaled(Vector2(stretch_shrink, stretch_shrink))
		x_form *= scale_xf
		
	var ev = event.xformed_by(x_form.affine_inverse())
	
	for node in get_children():
		if not node.is_class("Viewport"):
			continue
		elif node.gui_disable_input:
			continue
		
		node.unhandled_input(ev)
