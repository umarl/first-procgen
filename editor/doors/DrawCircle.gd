extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	update()

func _draw() -> void:
	draw_circle(Vector2.ZERO, 32, Color.black)
	draw_arc(Vector2.ZERO, 30, 0, TAU, 64, Color.white, 2, false)
