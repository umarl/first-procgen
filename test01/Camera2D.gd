extends Camera2D


var speed = 600

func _ready() -> void:
	set_process(true)

func _process(delta: float) -> void:
	var move = Vector2.ZERO
	
	if Input.is_action_pressed("ui_up"): move.y -= 1
	if Input.is_action_pressed("ui_down"): move.y += 1
	if Input.is_action_pressed("ui_left"): move.x -= 1
	if Input.is_action_pressed("ui_right"): move.x += 1
	
	position += move.normalized() * speed * delta
