extends Node2D

const N = 0b0001
const S = 0b0010
const W = 0b0100
const E = 0b1000

var room_doors = {
	Vector2(0, -1): N, Vector2(0, 1): S,
	Vector2(-1, 0): W, Vector2(1, 0): E
}

const ROOM_SIZE = 96
const map_width = 20
const map_height = 15

onready var Rooms = $Rooms

var map: Array = []

var RoomScene = preload("res://test01/blockbin.tscn")

func _ready() -> void:
	randomize()
	make_maze()
	draw_map()


func check_neighbors(room, unvisited) -> Array:
	var list = []
	for n in room_doors.keys():
		if room + n in unvisited:
			list.append(room + n)
	return list


func set_tile(tile: Vector2, id: int) -> void:
	if tile.x < 0 or tile.x >= map_width or tile.y < 0 or tile.y >= map_height:
		return
	map[tile.x][tile.y] = id


func get_tile(tile: Vector2) -> int:
	if tile.x < 0 or tile.x >= map_width or tile.y < 0 or tile.y >= map_height:
		return -1
	return map[tile.x][tile.y]


func make_maze() -> void:
	var unvisited = [] # array of unvisited tiles
	var stack = []
	
	# create the map and fill with solid tiles
	for column in range(map_width):
		map.append([])
		for row in range(map_height):
			map[column].append(0)
			unvisited.append(Vector2(column, row))
	
	var current = Vector2(0, 0)
	unvisited.erase(current)
	
	
#	while not unvisited.empty():
	while unvisited:
		var neighbors = check_neighbors(current, unvisited)
		
		if neighbors.size() > 0:
			var next = neighbors[randi() % neighbors.size()]
			stack.append(current)
			# add doors to both cells
			var dir = next - current
			var current_doors = get_tile(current) + room_doors[dir]
			var next_doors = get_tile(next) + room_doors[-dir]
			set_tile(current, current_doors)
			set_tile(next, next_doors)
			current = next
			unvisited.erase(next)
		elif stack:
			current = stack.pop_back()

func draw_map() -> void:
	for x in map_width:
		for y in map_height:
			var new_room = RoomScene.instance()
			new_room.frame = get_tile(Vector2(x, y))
			new_room.position = Vector2(x, y) * ROOM_SIZE
#			new_room.modulate = Color.from_hsv(randf(), 0.1, 1)
			Rooms.add_child(new_room)
